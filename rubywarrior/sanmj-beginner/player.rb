class Player
  def play_turn(warrior)
	@health = warrior.health unless @health
	if !(warrior.feel(:backward).wall?) && !(warrior.feel(:backward).captive?)
		warrior.walk!(:backward)
	else
		if (warrior.feel.captive?) 
			warrior.rescue!
		else
			if (warrior.feel(:backward).captive?)	
				warrior.rescue!(:backward)
			else
				if (warrior.feel.empty?) 
					if(warrior.health < 20) && (warrior.health >=@health)
						warrior.rest!
					else 
						warrior.walk!	
					end
				else 
					warrior.attack!
			end
		end
	end
	@health=warrior.health
   end
end
end
